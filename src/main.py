import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import createTrainingDataSet as ctd
import datetime
import json
from tensorflow.keras import backend as K
from math import radians, cos, sin, asin, sqrt, pi
import shutil, os
import argparse
import csv

def parseArguments():
	parser = argparse.ArgumentParser(description="parse args for project: taxi trajectory prediction i")
	parser.add_argument('--trainSize', type=int, default=1661952, help='Decide the size of traning dataset')
	parser.add_argument('--testSize', type=int, default=-1, help='Decide the size of test dataset')
	parser.add_argument('--trainingPath', nargs='?', default='data/pkdd-15-predict-taxi-service-trajectory-i/processed_train.json', help='Path of training data')
	parser.add_argument('--testPath', nargs='?', default='data/pkdd-15-predict-taxi-service-trajectory-i/processed_test.json', help='Path of test data')
	parser.add_argument('--batchSize', type=int, default=512, help='Mini-batch size')
	parser.add_argument('--lstmActivation', nargs='?', default='relu', help='Activation function of lstm')
	parser.add_argument('--epoch', type=int, default=50, help='Number of epochs')
	parser.add_argument('--lstmUnits', type=int, default=128, help="LSTM layer's output dim")
	parser.add_argument('--losses', nargs='?', default='haversine', help='Loss function')
	parser.add_argument('--opt', nargs='?', default='adam', help='Optimizer used during backward propergation')
	parser.add_argument('--metrics', nargs='?', default='mse', help='Metrics used to evaluate performance')
	parser.add_argument('--dropout', type=float, default=0, help='Dropout rate of lstm layer')
	return parser.parse_args()

def saveCurrentResultData():
	# Log files
	shutil.rmtree('result/currentLog')
	shutil.copytree('result/log/' + startTime, 'result/currentLog/')
	# Predict data for kepler.gl
	shutil.rmtree('result/currentPredict')
	os.mkdir('result/currentPredict')
	shutil.copy('result/predict/' + startTime + '.json', 'result/currentPredict/')

def haversine(y_label, y_predict):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    y_label = y_label * pi / 180
    y_predict = y_predict * pi / 180

    lon1 = tf.gather(y_label, [0], axis=1)
    lon2 = tf.gather(y_predict, [0], axis=1)
    lat1 = tf.gather(y_label, [1], axis=1)
    lat2 = tf.gather(y_predict, [1], axis=1)

    # haversine formula
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = K.sin(dlat/2)**2 + (K.cos(lat1) * K.cos(lat2) * K.sin(dlon/2)**2)
    c = 2 * tf.math.asin(K.sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    res = K.mean(c * r)
    return res

def normalize(data):
	data = np.array(data, dtype=object)
	epsilon = 10**(-8)
	numberOfPairs = sum([len(path) for path in data])
	sumOfPairs = np.sum([np.sum(path, axis=0) for path in data], axis=0) # (Long, Lat)
	mu = sumOfPairs / (numberOfPairs + epsilon)
	for i in range(len(data)):
		data[i] = data[i] - mu
	variance = np.sum([np.sum(path, axis=0) for path in data ** 2], axis=0) / (numberOfPairs + epsilon)
	sd = variance ** 0.5
	for i in range(len(data)):
		data[i] = data[i] / variance
	return data, mu, variance

def mapTestData(data, mu, variance):
	data = np.array(data, dtype=object)
	for i in range(len(data)):
		data[i] = (data[i] - mu) / variance
	return data

def prepareData(trainPath, trainSize, testPath, testSize):
	numOfTrainData = trainSize * 8 // 10
	numOfValidData = (trainSize - numOfTrainData) // 2
	numOfTestData = trainSize - (numOfTrainData + numOfValidData)

	if trainingPath[-3:] == 'csv':
		trainingDataset = ctd.getRowTrainingDataset(trainPath, trainSize)
		testDataset = ctd.getRowTestDataset(testPath, testSize)
		geoDataIndex = 8
		labelIndex = 9
	else:
		# Read data in json file 
		trainingDataset = ctd.getTrainingDataset(trainPath, trainSize)
		testDataset = ctd.getTestDataset(testPath, testSize)
		geoDataIndex = 1
		labelIndex = 2

	real_test_tripID = testDataset[0]

	trainData_x = trainingDataset[geoDataIndex][:numOfTrainData]
	train_x, mu, variance = normalize(trainData_x)
	train_x = tf.ragged.constant(train_x)
	validData_x = trainingDataset[geoDataIndex][numOfTrainData:numOfTrainData+numOfValidData]
	valid_x = tf.ragged.constant(validData_x)
	fake_testData_x = trainingDataset[geoDataIndex][numOfTrainData+numOfValidData:]
	fake_test_x = mapTestData(fake_testData_x, mu, variance)
	fake_test_x = tf.ragged.constant(fake_test_x)
	real_testData_x = testDataset[geoDataIndex]
	real_test_x = mapTestData(real_testData_x, mu, variance)
	real_test_x = tf.ragged.constant(real_test_x)

	trainData_y = trainingDataset[labelIndex][:numOfTrainData]
	train_y = tf.convert_to_tensor(trainData_y)
	validData_y = trainingDataset[labelIndex][numOfTrainData:numOfTrainData+numOfValidData]
	valid_y = tf.convert_to_tensor(validData_y)
	fake_test_y = trainingDataset[labelIndex][numOfTrainData+numOfValidData:]

	return train_x, train_y, valid_x, valid_y, fake_test_x, fake_test_y, real_test_x, real_test_tripID

def buildModel(LSTMUnits, batchSize, lstmActivation):
	initializer = tf.keras.initializers.GlorotUniform(seed=1)
	recurrent_initializer = tf.keras.initializers.Orthogonal(seed=2)

	model = keras.Sequential()
	model.add(layers.LSTM(units=LSTMUnits, 
		batch_input_shape=(batchSize, None, 2), 
		kernel_initializer=initializer,
		activation=lstmActivation, 
		recurrent_regularizer='l2',
		recurrent_initializer=recurrent_initializer))
	model.add(layers.Dropout(rate=dropoutRate,
		seed=3))
	model.add(layers.Dense(units=2, 
		kernel_initializer=initializer, 
		kernel_regularizer='l2'))
	model.summary()
	return model

def compileModel(model, loss, optimizer, metrics):
	model.compile(
		loss=loss,
		optimizer=optimizer,
		metrics=metrics
	)
	return model

def trainModel(model, train_x, train_y, valid_x, valid_y, batchSize, numOfEpochs):
	logdir = "result/log/" + startTime
	checkpoint_filepath = "result/checkpoint/" + startTime

	model_checkpoint_callback = keras.callbacks.ModelCheckpoint(
		filepath=checkpoint_filepath,
		save_weights_only=True,
		monitor='val_loss',
		mode='min',
		save_best_only=True)

	tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
	file_writer = tf.summary.create_file_writer(logdir + "/metrics")
	file_writer.set_as_default()

	model.fit(
    	train_x, 
    	train_y, 
    	validation_data=(valid_x, valid_y), 
    	batch_size=batchSize, 
    	epochs=numOfEpochs, 
    	callbacks=[tensorboard_callback, model_checkpoint_callback]
	)
	#print('model.outputs', K.eval(model.outputs[0]))
	model.load_weights(checkpoint_filepath)

	model.save(f'result/model/lstmUnits{LSTMUnits}_dropout{dropoutRate}_{startTime}')
	model.save('result/currentModel')
	return model

def generateFakePredictJson(results, labels):
	resultJson = dict(type='FeatureCollection', features=[])
	for i in range(len(results)):
		featureDict = dict(type='Feature', geometry=dict(type='LineString', coordinates=[]), properties=dict())
		results[i] = [results[i][0].item(), results[i][1].item()]
		featureDict['geometry']['coordinates'] = [results[i], labels[i]]
		featureDict['properties']['index'] = i
		featureDict['properties']['predictLong'] = results[i][0]
		featureDict['properties']['predictLat'] = results[i][1]
		featureDict['properties']['labelLong'] = labels[i][0]
		featureDict['properties']['labelLat'] = labels[i][1]
		resultJson['features'].append(featureDict)
	filename = startTime + '.json'
	with open('result/predict/' + filename, 'w') as w:
		w.write(json.dumps(resultJson))

def generatePredictCSV(results, tripID):
	filename = f'lstmUnits{LSTMUnits}_dropout{dropoutRate}_{startTime}.csv'
	row_list = list(map(lambda x,y: [x, y[1], y[0]], tripID, results))

	with open('result/predict/' + filename, 'w', newline='') as w:
		writer = csv.writer(w)
		writer.writerow(["TRIP_ID", "LATITUDE", "LONGITUDE"]) # TRIP_ID, LATITUDE, LONGITUDE
		writer.writerows(row_list)



if __name__ == '__main__':
	# dictionaries
	lossesDict = {'haversine': haversine,
				  'mse': tf.keras.metrics.MeanSquaredError()}
	metricsDict = {'haversine': haversine,
				  'mse': tf.keras.metrics.MeanSquaredError()}

	# parse data
	args = parseArguments()
	trainSize = args.trainSize
	testSize = args.testSize
	trainingPath = args.trainingPath
	testPath = args.testPath
	batchSize = args.batchSize
	lstmActivation = args.lstmActivation
	numOfEpochs = args.epoch
	LSTMUnits = args.lstmUnits
	dropoutRate = args.dropout
	optimizer = args.opt
	losses = lossesDict[args.losses] #
	metrics = [metricsDict[args.metrics]] #
	startTime = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
	print(f"Hyperparameters: {args}")

	# prepare data for training
	(train_x, train_y, valid_x, valid_y, fake_test_x, fake_test_y, real_test_x, real_test_tripID) = prepareData(trainingPath, trainSize, testPath, testSize)

	# build model
	model = buildModel(LSTMUnits=LSTMUnits, batchSize=batchSize, lstmActivation=lstmActivation)

	# compile model
	model = compileModel(model, losses, optimizer, metrics)

	# train and save model
	model = trainModel(model, train_x, train_y, valid_x, valid_y, batchSize, numOfEpochs)

	# predict
	fake_results = model.predict(fake_test_x)
	real_results = model.predict(real_test_x)

	# prepare predict data for kepler.gl
	generateFakePredictJson(list(fake_results), fake_test_y)
	
	# output the real prediction csv file
	generatePredictCSV(list(real_results), real_test_tripID)

	# save current log
	saveCurrentResultData()