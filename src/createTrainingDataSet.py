import sys
import csv
import json
import random
import time
random.seed(30)

def randomlySelectTrajectory(path):
	totalPartitionNumbers = 3
	partitionLength = len(path) // totalPartitionNumbers
	partitionIndex = random.randint(0, totalPartitionNumbers-1)
	start = partitionLength * partitionIndex
	end = start + partitionLength
	return path[start:end]

def getRowTrainingDataset(path, trainingDataSize):
	with open(path, 'r') as f:
		csv_reader = csv.reader(f, delimiter=',')
		header = next(csv_reader)
		trainingDataset = [[] for i in range(10)]
		counter = 0
		for row in csv_reader:
			if row[7] == 'True' or row[8] == '[]':
				continue
			for j in range(8):
				trainingDataset[j].append(row[j])
			geoData = list(map(lambda x: list(map(lambda y: float(y), x.split(","))), row[8][2:-2].split("],[")))
			if len(geoData) < 4: # The shortest path should be [[pair1], [pair2], [pair3], [label]]
				continue
			partialGeoData = randomlySelectTrajectory(geoData[0:-1])
			trainingDataset[8].append(partialGeoData) # partial path list
			trainingDataset[9].append(geoData[-1]) # label pair (long, lat)

			counter += 1
			if counter == trainingDataSize:
				break
	return trainingDataset

def getRowTestDataset(path, testDataSize):
	with open(path, 'r') as f:
		csv_reader = csv.reader(f, delimiter=',')
		header = next(csv_reader)
		testDataset = [[] for i in range(9)]
		counter = 0
		for row in csv_reader:
			if row[7] == 'True' or row[8] == '[]':
				continue
			for j in range(8):
				testDataset[j].append(row[j])
			geoData = list(map(lambda x: list(map(lambda y: float(y), x.split(","))), row[8][2:-2].split("],[")))
			testDataset[8].append(geoData)

			counter += 1
			if counter == testDataSize:
				break
	return testDataset

def getTrainingDataset(path, trainDataSize):
	"""
		Get the pre-processed data, which is stored in a json file.
		Return a list

		path: path of the pre-processed training data
		trainDataSize: total numbers of training data
	"""
	startTime = time.time()
	with open(path, 'r') as f:
		trainData = json.loads(f.read())
		if trainDataSize == -1:
			trainData = [trainData['tripID'], trainData['geoData'], trainData['label']]
		else:
			trainData = [trainData['tripID'][:trainDataSize], trainData['geoData'][:trainDataSize], trainData['label'][:trainDataSize]]
	endTime = time.time()
	diffTime = endTime - startTime
	# print(f'get training dataset spent {diffTime} seconds')
	return trainData

def getTestDataset(path, testDataSize):
	"""
		Get the pre-processed data, which is stored in a json file.
		Return a list

		path: path of the pre-processed test data
		testDataSize: total numbers of test data
	"""
	startTime = time.time()
	with open(path, 'r') as f:
		testData = json.loads(f.read())
		if testDataSize == -1:
			testData = [testData['tripID'], testData['geoData']]
		else:
			testData = [testData['tripID'][:testDataSize], testData['geoData'][:testDataSize]]
	endTime = time.time()
	diffTime = endTime - startTime
	# print(f'get test dataset spent {diffTime} seconds')
	return testData


def prepareDataAndStoreToJson():
	trainPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/train.csv'
	testPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/test.csv'
	takeAllData = -1

	train = getRowTrainingDataset(trainPath, takeAllData)
	test = getRowTestDataset(testPath, takeAllData)

	
	train_dict = {'tripID': train[0], 'geoData': train[8], 'label': train[9]}
	test_dict = {'tripID': test[0], 'geoData': test[8]}

	availableTrainSize = len(train[0])
	availableTestSize = len(test[0])

	saveTrainPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/processed_train.json'
	saveTestPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/processed_test.json'

	with open(saveTrainPath, 'w') as w:
		json.dump(train_dict, w)
	with open(saveTestPath, 'w') as w:
		json.dump(test_dict, w)

	'''
	train = list(map(lambda x, y, z: [x, y, z], train[0], train[8], train[9]))
	test = list(map(lambda x, y: [x, y], test[0], test[8]))

	saveTrainPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/processed_train.csv'
	with open(saveTrainPath, 'w', newline='') as w:
		writer = csv.writer(w)
		writer.writerows(train)

	saveTestPath = 'data/pkdd-15-predict-taxi-service-trajectory-i/processed_test.csv'
	with open(saveTestPath, 'w', newline='') as w:
		writer = csv.writer(w)
		writer.writerows(test)
	'''
	return availableTrainSize, availableTestSize

if __name__ == '__main__':
	(availableTrainSize, availableTestSize) = prepareDataAndStoreToJson()
	print(f'availableTrainSize: {availableTrainSize}')
	print(f'availableTestSize: {availableTestSize}')


	
