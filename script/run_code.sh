# !/bin/bash

echo '[INFO] This script is used to run the main python code.'
echo '[INFO] Activate the virtual environment.'
source ttpi/bin/activate

echo '[INFO] Create small training dataset.'
python3 src/createTrainingDataSet.py 1000 > data/trainingDataSet1000.txt

echo '[INFO] Open jupyter notebook.'
jupyter notebook

echo '[INFO] Deactivate the virtual environment.'
deactivate

echo '[INFO] End.'