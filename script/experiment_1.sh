# !/bin/bash
update_training_result () {
	git add .
	git commit -m "get training result - $1"
	git push
}

source ttpi/bin/activate
# python3 src/main.py --lstmUnits 32 --dropout 0 --trainSize 10 --testSize 1 --epoch 2 | tee exp_lstmUnits32.txt

# --lstmUnits 32 64 128 256 512
python3 src/main.py --lstmUnits 32 | tee result/trainingDetail/exp_lstmUnits32.txt
update_training_result "lstmUnits 32"
python3 src/main.py --lstmUnits 64 | tee result/trainingDetail/exp_lstmUnits64.txt
update_training_result "lstmUnits 64"
python3 src/main.py --lstmUnits 128 | tee result/trainingDetail/exp_lstmUnits128.txt
update_training_result "lstmUnits 128"
python3 src/main.py --lstmUnits 256 | tee result/trainingDetail/exp_lstmUnits256.txt
update_training_result "lstmUnits 256"
python3 src/main.py --lstmUnits 512 | tee result/trainingDetail/exp_lstmUnits512.txt
update_training_result "lstmUnits 512"

# --dropout 0 to 1
python3 src/main.py --dropout 0 | tee result/trainingDetail/exp_dropout0.txt
update_training_result "dropout 0"
python3 src/main.py --dropout 0.1 | tee result/trainingDetail/exp_dropout0.1.txt
update_training_result "dropout 0.1"
python3 src/main.py --dropout 0.2 | tee result/trainingDetail/exp_dropout0.2.txt
update_training_result "dropout 0.2"
python3 src/main.py --dropout 0.3 | tee result/trainingDetail/exp_dropout0.3.txt
update_training_result "dropout 0.3"
python3 src/main.py --dropout 0.4 | tee result/trainingDetail/exp_dropout0.4.txt
update_training_result "dropout 0.4"
python3 src/main.py --dropout 0.5 | tee result/trainingDetail/exp_dropout0.5.txt
update_training_result "dropout 0.5"
python3 src/main.py --dropout 0.6 | tee result/trainingDetail/exp_dropout0.6.txt
update_training_result "dropout 0.6"
python3 src/main.py --dropout 0.7 | tee result/trainingDetail/exp_dropout0.7.txt
update_training_result "dropout 0.7"
python3 src/main.py --dropout 0.8 | tee result/trainingDetail/exp_dropout0.8.txt
update_training_result "dropout 0.8"
python3 src/main.py --dropout 0.9 | tee result/trainingDetail/exp_dropout0.9.txt
update_training_result "dropout 0.9"
python3 src/main.py --dropout 1 | tee result/trainingDetail/exp_dropout1.txt
update_training_result "dropout 1"

deactivate
