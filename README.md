# Taxi Trajectory Prediction I

A Kaggle challenge: https://www.kaggle.com/c/pkdd-15-predict-taxi-service-trajectory-i/overview


## Log 
### 2020/06/27
1. Read task discription
2. Download dataset
3. Convert test data from csv to geojson
4. Visualize the test data by using keplergl

### 2020/06/28
1. Look into training data
2. Visualize the first 100 rows of training data by using keplergl

### 2020/07/05
1. Modify createTrainingDataset.py
2. Separate code into different functions
3. Set up total number of data as a parameter "dataSize"
4. dataSize includes training, validating and testing
5. Modify setupvenv.sh to upgrade the version of pip3 and setuptools
6. Add main.py

### 2020/07/06
1. Impletment a Keras model which comebines a LSTM and a Dense layer
2. Manually generate fake data and ground truth (only one dimension) for testing the model
3. Replace the cost function from SparseCategoricalCrossEntropy to CategoricalCrossEntropy to solve the problem of inconsistent output shape

### 2020/07/07
1. Play around different loss functions and metrics
2. Replace both loss function and metrics to MSE
3. Adjust batch size to 8
4. Adjust the proportioin of training dataset and dev dataset to 8:2
5. Test the model with real data

### 2020/07/09
1. Separate the original training dataset into train, valid and test dataset (because we don't have the ground truth of the original test dataset)
2. Play aroung TensorBoard
3. Try to ouput predictions with the new test data
4. Save the predictions in GeoJSON format, but it failed :p

### 2020/07/10
1. Fix the GeoJSON format problem
2. Visualize the predictions and ground truth on a map
3. Try to solve the problem of high bias by adding an Embedding layer

### 2020/07/11
1. Give up the idea of adding an Embedding layer
2. Map the original data to the range between 0 and 1
3. Implement a self-defined loss function (Haversine Distance)

### 2020/07/15
1. Implement normalization function for non-fixed length training data
2. Use the same mu and variance, which is computed from training data, to scale fake test data

### 2020/07/21
1. Add dropout layer right after lstm layer
2. There are at least 1662144 row of data have more then 3 pair of geo data
3. Found a issue: the program broke if we use all training data to train model

### 2020/07/25
1. Parse arguments from terminal
2. Normalize the real test data
3. Make predictions based on real test data 
